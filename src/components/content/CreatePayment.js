import React from "react";
import axios from 'axios'
import classNames from "classnames";
import {
  faHome,
  faBriefcase,
  faPaperPlane,
  faQuestion,
  faImage,
  faCopy,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import NavBar from "./Navbar";
import {
  Nav,
  Button,
  Row,
  Col,
  Container,
  InputGroup,
  FormControl,
  Form,
} from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Admin from "../admin/index";
import GetAPIData from "./GetAPIData"
import PostAPI from "./PostAPI"
import PaymentFactory from "./PaymentFactory"
import Payment from "./Payment.css"
class CreatePayment extends React.Component {
  constructor(props) {
    super(props);
console.log(this.props)
    this.state = {
      isOpen: false,
      isMobile: true,
      arrpaymenttype:[],
      arrsitetype:[],
      arrvisittype:[],
      arrcrftype:[],
      PaymentTypeName:this.props.paytypeName
      

    };
   
    this.previousWidth = -1;
  }
    
	  componentDidMount() {
      GetAPIData("http://localhost:55728/api/GetPaymentTypes").then((x) =>
      this.setState({
      arrpaymenttype: [{PaymentTypeId: '', PaymentType: '--Select--'}].concat(x),
      PaymentTypeId:x.find(arr=>arr.PaymentType==this.props.paytypeName).PaymentTypeId,
      
       }),
    );

    GetAPIData("http://localhost:55728/api/GetSites").then((x) =>
    this.setState({
      arrsitetype: [{SiteTypeId: '', SiteType: '--Select--'}].concat(x)
     })
  );

  GetAPIData("http://localhost:55728/api/GetVisitTypes").then((x) =>
    this.setState({
      arrvisittype: [{VisitTypeId: '', VisitType: '--Select--'}].concat(x)
     })
  );

  GetAPIData("http://localhost:55728/api/GetCRFTypes").then((x) =>
  this.setState({
    arrcrftype: [{CRFTypeId: '', CRFType: '--Select--'}].concat(x)
   })
);
 
  }
  changeHandler=(e)=>{
    
    this.setState({[e.target.name]:e.target.value})
    
  }
   
  submitHandler=e=>{
    e.preventDefault()
    var paymentFactory = new PaymentFactory();      
    let payment=paymentFactory.create(this.props.paytypeName,this.state); 
      PostAPI('http://localhost:55728/api/Posttbl_Payments', payment).then((x) =>   
      {
        alert(x);     
       let clear= paymentFactory.clearmodel(); 
       this.setState(clear);       
      }, 
     );

     
  }
  render() {
   
    const{site,
         visit,
         PaymentTypeId,
         arrpaymenttype,
         arrsitetype,
         arrvisittype,
         arrcrftype,
         CRF,
         paymentstatus,
         amount,
         Description,
         PaymentTypeName,     
         }=this.state
  
    return (
      <React.Fragment>
        {" "}
        <Container
          fluid
          className={classNames("content", {
            "is-open": this.props.isOpen,
          })}
          style={{
            padding: "0px",
            width: "103%",
            border: "1px solid #c6cbc6",
            margin: "0px 0px 0px 10px",
          }}
        >
          <NavBar toggle={this.props.toggyle} />
          <div>
            <div style={{ color: "black", padding: "10px" }}>
              <Form onSubmit={this.submitHandler}>
              <Form.Group >
                
                <Form.Row className="p-10">
                  <Form.Label column="sm" lg={2}>
                    Site:
                  </Form.Label>
                  <Col>
                    <Form.Control as="select" custom name="SiteTypeId" value={this.state.SiteTypeId} onChange={this.changeHandler}>
                    {                               
                     (arrsitetype && arrsitetype.length > 0) && arrsitetype.map((types) => {
                       return (                                
                        <option value={types.SiteTypeId}>{types.SiteType}</option>);
                        })
                      }
                    </Form.Control>
                  
                  </Col>
                </Form.Row>                
                <Form.Row  className="p-10">
                  <Form.Label column="sm" lg={2}>
                    Payment Type:
                  </Form.Label>
                  <Col>  
                 
                    <Form.Control as="select" custom name="PaymentTypeId" value={this.state.PaymentTypeId}  onChange={this.changeHandler} disabled="disabled">
                   {                                     
                   (arrpaymenttype && arrpaymenttype.length > 0) && arrpaymenttype.map((ptypes) => {
                    return (                     
                    <option  value={ptypes.PaymentTypeId}>{ptypes.PaymentType}</option>);
                     })
                    }
                    </Form.Control>
                  </Col>
                </Form.Row>
                <Form.Row className="p-10">
                  <Form.Label column="sm" lg={2}>
                    Visit:
                  </Form.Label>
                  <Col>
                    <Form.Control as="select" custom name="VisitTypeId" value={this.state.VisitTypeId} onChange={this.changeHandler}>
                    {                               
                     (arrvisittype && arrvisittype.length > 0) && arrvisittype.map((types) => {
                       return (                                
                        <option value={types.VisitTypeId}>{types.VisitType}</option>);
                        })
                      }
                    </Form.Control>
                  </Col>
                </Form.Row>
                <Form.Row className="p-10">
                  <Form.Label column="sm" lg={2}>
                    CRF:
                  </Form.Label>
                  <Col>
                    <Form.Control as="select" custom name="CRFTypeId" value={this.state.CRFTypeId} onChange={this.changeHandler} >
                    {                               
                     (arrcrftype && arrcrftype.length > 0) && arrcrftype.map((types) => {
                       return (                                
                        <option value={types.CRFTypeId}>{types.CRFType}</option>);
                        })
                      }
                    </Form.Control>
                  </Col>
                </Form.Row>
                <Form.Row className="p-10">
                  <Form.Label column="sm" lg={2}>
                    Status:
                  </Form.Label>
                  <Col>
                    <Form.Check
                      inline
                      type="radio"
                      id="complete"
                      label="Complete"
                      name="Status"
                      value={'Complete'}
                      onChange={this.changeHandler}
                    />
                    <Form.Check
                      inline
                      type="radio"
                      id="Approved"
                      label="Approved"
                      name="Status"
                      value={'Approved'}
                      onChange={this.changeHandler}
                    />
                    <Form.Check
                      inline
                      type="radio"
                      id="Signed"
                      label="Signed"
                      name="Status"
                      value={'Signed'}
                      onChange={this.changeHandler}
                    />
                    <Form.Check
                      inline
                      type="radio"
                      id="Locked"
                      label="Locked"
                      name="Status"
                      value={'Locked'}
                      onChange={this.changeHandler}
                    />
                  </Col>
                </Form.Row>
                <Form.Row className="p-10">
                  <Form.Label column="sm" lg={2} >
                    Amount:
                  </Form.Label>
                  <Col>
                    <Form.Control name="Amount" value={amount} onChange={this.changeHandler}/>
                  </Col>
                </Form.Row>
                <Form.Row className="p-10">
                  <Form.Label column="sm" lg={2} >
                    Description:
                  </Form.Label>
                  <Col>
                    <Form.Control as="textarea" rows="3" name="Desc" value={Description} onChange={this.changeHandler}/>
                  </Col>
                </Form.Row>
                <br />
                 
              </Form.Group>
              <div style={{ textAlign: "right", margin: "10px" }}>
                <Button type="submit" variant="primary" >Submit</Button>{" "}
                <Button variant="primary">Cancel</Button>{" "}
              </div>
              </Form>
            </div>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

export default CreatePayment;
