import React from 'react'

function PaymentFactory() {
    this.create=function(name,model)
    {
        return new Payment(name,model);
    }
    this.clearmodel=function()
    {
        return new Clear();
    }
}
function Payment(name,model) {
    if(name=="Study Payments")
    {
        const payment={
            SiteTypeId:model.SiteTypeId,
            PaymentTypeId:model.PaymentTypeId,
            VisitTypeId:model.VisitTypeId,
            CRFTypeId:model.CRFTypeId,
            Status:model.Status,
            Amount:model.Amount,
            Desc:model.Desc,
            PaymentTypeName:model.PaymentTypeName,
            
          }
          return payment
    }
    if(name=="Site Payments")
    {
        const payment={
            SiteTypeId:model.SiteTypeId,
            PaymentTypeId:model.PaymentTypeId,
            VisitTypeId:model.VisitTypeId,
            CRFTypeId:model.CRFTypeId,
            Status:model.Status,
            Amount:model.Amount,
            Desc:model.Desc,
            PaymentTypeName:model.PaymentTypeName,
            
          }
          return payment
    }
    if(name=="Visit/CRF Payments")
    {
        const payment={
            SiteTypeId:model.SiteTypeId,
            PaymentTypeId:model.PaymentTypeId,
            VisitTypeId:model.VisitTypeId,
            CRFTypeId:model.CRFTypeId,
            Status:model.Status,
            Amount:model.Amount,
            Desc:model.Desc,
            PaymentTypeName:model.PaymentTypeName,
            
          }
          return payment
    }
}
function Clear()
{
    const payment={
        SiteTypeId:'',
        VisitTypeId:'',
        CRFTypeId:'',
        Status:'',
        amount:'',
        Description:'',
        PaymentTypeName:'',
        
      }
      return payment
}
export default PaymentFactory
