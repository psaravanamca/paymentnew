import React from 'react'
import axios from 'axios'
function PostAPI(URL,objModel) {
 
     return new Promise((resolve, reject) => {
        axios
          .post(URL,objModel)
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            console.log(error.response);
            reject(error);
          });
      });
    
}

export default PostAPI
