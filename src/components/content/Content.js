import React from "react";
import classNames from "classnames";
import {
  faHome,
  faBriefcase,
  faPaperPlane,
  faQuestion,
  faImage,
  faCopy,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import NavBar from "./Navbar";
import { Nav, Button, Row, Col, Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CreatePayment from "./CreatePayment";
// import SideBar from "../sidebar/SideBar";
class Content extends React.Component {
  constructor(props) {
    super(props);

    // Moblie first
    this.state = {
      isOpen: false,
      isMobile: true,
    };

    this.previousWidth = -1;
  }

  updateWidth() {
    const width = window.innerWidth;
    const widthLimit = 576;
    const isMobile = width <= widthLimit;
    const wasMobile = this.previousWidth <= widthLimit;

    if (isMobile !== wasMobile) {
      this.setState({
        isOpen: !isMobile,
      });
    }

    this.previousWidth = width;
  }

  /**
   * Add event listener
   */
  componentDidMount() {
    this.updateWidth();
    window.addEventListener("resize", this.updateWidth.bind(this));
  }

  /**
   * Remove event listener
   */
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWidth.bind(this));
  }

  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };
  render() {
    return (
      <React.Fragment>
        <div style={{ backgroundColor: "#ffffff", padding: "5px" }}>
          {" "}
          <Router>
            <Row style={{ marginTop: "10px" }}>
              <Col sm={3} style={{ maxWidth: "20%" }}>
                {/* <SideBar toggle={this.toggle} isOpen={this.state.isOpen} /> */}
                <div
                  className={classNames("sidebar", {
                    "is-open": this.props.isOpen,
                  })}
                >
                  <div
                    className="sidebar-header"
                    style={{
                      height: "35px",
                      textAlign: "center",
                      paddingTop: "6px",
                    }}
                  >
                    NAVIGATION
                  </div>

                  <Nav className="flex-column pt-2">
                    <p className="ml-3">
                      <b>Payments</b>
                    </p>

                    <Nav.Item>
                      <Nav.Link>
                        <FontAwesomeIcon icon={faHome} className="mr-2" />
                        <Link to={"/CreatePayment"}>
                          <span>{"View Payments"}</span>
                        </Link>
                      </Nav.Link>
                    </Nav.Item>

                    <Nav.Item>
                      <Nav.Link>
                        <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                        <Link to={"/"}>
                          <span>{"Create Study Payments"}</span>
                        </Link>
                      </Nav.Link>
                    </Nav.Item>

                    <Nav.Item>
                      <Nav.Link>
                        <FontAwesomeIcon icon={faImage} className="mr-2" />
                        <Link to={"/CreatePaymentSite"}>
                          <span>{"Create Site Payments"}</span>
                        </Link>
                      </Nav.Link>
                    </Nav.Item>

                    <Nav.Item>
                      <Nav.Link>
                        <FontAwesomeIcon icon={faQuestion} className="mr-2" />
                        <Link to={"/CreatePaymentVisit"}>
                          <span>{"Create Visit/CRF Payments"}</span>
                        </Link>
                      </Nav.Link>
                    </Nav.Item>
                  </Nav>
                </div>
              </Col>
              <Col sm={9} style={{ padding: "0px", width: "80%" }}>
                 
                <Route key={1} path="/" exact={true}>
                  <CreatePayment  paytypeName={"Study Payments"}/>
                </Route>
                <Route key={1} path="/CreatePaymentSite" exact={true}>
                  <CreatePayment   paytypeName={"Site Payments"}/>
                </Route>
                <Route key={1} path="/CreatePaymentVisit" exact={true}>
                  <CreatePayment  paytypeName={"Visit/CRF Payments"}/>
                </Route>
                
              </Col>
            </Row>
          </Router>
        </div>
      </React.Fragment>
    );
  }
}

export default Content;
