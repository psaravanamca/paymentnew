 import React from 'react'
 import axios from 'axios'
 function GetAPIData(url) {
  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        console.log(error.response);
        reject(error);
      });
  });
 }
 
 export default GetAPIData
 