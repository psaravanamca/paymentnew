import React from "react";
import classNames from "classnames";
import { Nav, Button, Row, Col, Container } from "react-bootstrap";

import NavBar from "../content/Navbar";
class Admin extends React.Component {
  render() {
    return (
      <React.Fragment>      
        <Container
                fluid
                className={classNames("content", {
                  "is-open": this.props.isOpen,
                })}
                style={{ padding: "0px", width: "105%" }}
              >
                <NavBar toggle={this.props.toggle} />
              </Container>
      </React.Fragment>
    );
  }
}

export default Admin;
