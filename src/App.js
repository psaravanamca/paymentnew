import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
// import SideBar from "./components/sidebar/SideBar";
import Content from "./components/content/Content";
import Admin from "../src/components/admin/index";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import {
  Accordion,
  faHome,
  faBriefcase,
  faPaperPlane,
  faQuestion,
  faImage,
  faCopy,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import { Nav, Button, Row, Col, Container } from "react-bootstrap";

import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import classNames from "classnames";
class App extends React.Component {
  constructor(props) {
    super(props);

    // Moblie first
    this.state = {
      isOpen: false,
      isMobile: true,
    };

    this.previousWidth = -1;
  }

  updateWidth() {
    const width = window.innerWidth;
    const widthLimit = 576;
    const isMobile = width <= widthLimit;
    const wasMobile = this.previousWidth <= widthLimit;

    if (isMobile !== wasMobile) {
      this.setState({
        isOpen: !isMobile,
      });
    }

    this.previousWidth = width;
  }

  /**
   * Add event listener
   */
  componentDidMount() {
    this.updateWidth();
    window.addEventListener("resize", this.updateWidth.bind(this));
  }

  /**
   * Remove event listener
   */
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWidth.bind(this));
  }

  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    return (
      <Container
        style={{
          margin: "7px",
          padding: "0px",
          maxWidth: "99%",
          border: "1px solid #c6cbc6",
        }}
      >
        <Row>
          <Col>
            <div
              style={{
                fontSize: "x-large",
                color: "#3838b4",
                fontWeight: "bold",
                height: "50px",
                position: "relative",
                bottom: "-15px",
                paddingLeft: "10px",
              }}
            >
              Demo Mock App
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div>
              <div className="sidebar-header">
                <Router>
                  <Nav
                    activeKey="/home"
                    // onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
                  >
                    <Nav.Item style={{ color: "#ffffff" }}>
                      <Nav.Link>
                        <Link to={"/"}>
                          <span>{"Home"}</span>
                        </Link>
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item style={{ color: "#ffffff" }}>
                      <Nav.Link eventKey="link-1">
                        <Link to={"/home1"}>
                          <span>{"User Management"}</span>
                        </Link>
                      </Nav.Link>
                    </Nav.Item>
                  </Nav>
                  <Route key={1} path="/" exact={true}>
                    <Content toggle={this.toggle} isOpen={this.state.isOpen} />
                  </Route>
                  <Route key={1} path="/home1" exact={true}>
                    <Admin toggle={this.toggle} isOpen={this.state.isOpen} />
                  </Route>
                  {/* <Route key={1} path="/payment/view" exact={true}>
                    <Admin toggle={this.toggle} isOpen={this.state.isOpen} />
                  </Route> */}
                </Router>
              </div>
            </div>
          </Col>
        </Row>
        {/* <Row style={{ marginTop: "10px" }}>
          <Col sm={3}>
            <SideBar toggle={this.toggle} isOpen={this.state.isOpen} />
          </Col>
          <Col sm={9} style={{ padding: "0px" }}>
            <Content toggle={this.toggle} isOpen={this.state.isOpen} />
          </Col>
        </Row> */}
      </Container>
    );
  }
}

export default App;
